package com.example.project.androidcalculator1;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.Stack;

public class BaseConversion extends AppCompatActivity
{
    private Button buttonResult;
    private static RadioGroup radioG1, radioG2;
    private RadioButton radio, radio1, radio2, radio3, radio4, radio5, radio6, radio7, radio8;
    private TextView tvNumResult;
    private EditText edNumReceived;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base_conversion);

    }

    public void buttonListener(View view)
    {
        radioG1 = (RadioGroup)findViewById(R.id.radioG1);
        buttonResult = (Button)findViewById(R.id.button);
        edNumReceived = (EditText) findViewById(R.id.editText);
        tvNumResult = (TextView)findViewById(R.id.textView3);

        int selected_id = radioG1.getCheckedRadioButtonId();
        radio = (RadioButton) findViewById(selected_id);

        String number = edNumReceived.getText().toString();

        if(radio.getText().toString().equals("Binary"))
        {
            if(number.matches("[01]+"))
            {
                binaryConversion(number);
            }
            else{
                tvNumResult.setText("not binary");
            }
        }
        else if(radio.getText().toString().equals("Decimal"))
        {
            if(number.matches("[0123456789]+"))
            {
                decimalConversion(number);
            }
            else{
                tvNumResult.setText("not decimal");
            }
        }
        else if(radio.getText().toString().equals("Octal"))
        {
            if(number.matches("[01234567]+"))
            {
                octalConversion(number);
            }
            else{
                tvNumResult.setText("not octal");
            }
        }
        else if(radio.getText().toString().equals("Hex"))
        {
            if(number.matches("[0123456789abcdef]+"))
            {
                hexConversion(number);
            }
            else{
                tvNumResult.setText("not hex");
            }
        }
    }

    public void hexConversion(String decimal)
    {
        int num = Integer.parseInt(decimal, 16);
        String hexRep = "0123456789abcdef";
        radioG2 = (RadioGroup)findViewById(R.id.radioG2);
        int selected_id = radioG2.getCheckedRadioButtonId();
        radio = (RadioButton) findViewById(selected_id);

        if(radio.getText().toString().equals("Decimal"))
        {
            tvNumResult.setText("" + Integer.parseInt(decimal, 16));
        }
        else if(radio.getText().toString().equals("Octal"))
        {
            tvNumResult.setText("" + Integer.toOctalString(Integer.parseInt(decimal, 16)));
        }
        else if(radio.getText().toString().equals("Binary"))
        {
            tvNumResult.setText("" + Integer.toBinaryString(Integer.parseInt(decimal, 16)));
        }
    }

    public void octalConversion(String decimal)
    {
        radioG2 = (RadioGroup)findViewById(R.id.radioG2);
        int selected_id = radioG2.getCheckedRadioButtonId();
        radio = (RadioButton) findViewById(selected_id);

        if(radio.getText().toString().equals("Decimal"))
        {
            tvNumResult.setText("" + Integer.parseInt(decimal, 8));
        }
        else if(radio.getText().toString().equals("Hex"))
        {
            tvNumResult.setText("" + Integer.toHexString(Integer.parseInt(decimal, 8)));
        }
        else if(radio.getText().toString().equals("Binary"))
        {
            tvNumResult.setText("" + Integer.toBinaryString(Integer.parseInt(decimal,8)));
        }
    }

    public void decimalConversion(String decimal)
    {
        int num = Integer.parseInt(decimal, 10);

        radioG2 = (RadioGroup)findViewById(R.id.radioG2);
        int selected_id = radioG2.getCheckedRadioButtonId();
        radio = (RadioButton) findViewById(selected_id);

        if(radio.getText().toString().equals("Binary"))
        {
            Stack<Integer> stack = new Stack<>();

            while (num != 0)
            {
                int d = num % 2;
                stack.push(d);
                num /= 2;
            }

            String binary = "";
            while (!(stack.isEmpty() ))
            {
                binary += stack.pop();
            }
            tvNumResult.setText("" + binary);
        }

        if(radio.getText().toString().equals("Octal"))
        {
            tvNumResult.setText("" + Integer.toOctalString(num));
        }

        if(radio.getText().toString().equals("Hex"))
        {
            Integer io = Integer.valueOf(decimal);
            tvNumResult.setText("" + io.toHexString(num));
        }
    }


    public void binaryConversion(String binary)
    {
        int num = Integer.parseInt(binary, 2);

        radioG2 = (RadioGroup)findViewById(R.id.radioG2);
        int selected_id = radioG2.getCheckedRadioButtonId();
        radio = (RadioButton) findViewById(selected_id);

        if(radio.getText().toString().equals("Decimal"))
        {
            tvNumResult.setText("" + Integer.parseInt(binary,2));
        }

        if(radio.getText().toString().equals("Octal"))
        {
            tvNumResult.setText("" + Integer.toOctalString(num));
        }

        if(radio.getText().toString().equals("Hex"))
        {
            tvNumResult.setText("" + Integer.toHexString(num));
        }
    }
}
