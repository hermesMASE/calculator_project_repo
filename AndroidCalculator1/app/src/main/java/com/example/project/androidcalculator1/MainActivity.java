package com.example.project.androidcalculator1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivity extends Activity {

    private int[] buttonNumbers = {R.id.button0, R.id.button1, R.id.button2, R.id.button3, R.id.button4, R.id.button5, R.id.button6, R.id.button7, R.id.button8, R.id.button9};
    private int[] buttonOperators = {R.id.buttonAdd, R.id.buttonSubtract, R.id.buttonMultiply, R.id.buttonDivide};
    Button button2ndF,buttonALPHA,buttonSETUP,buttonLEFT,
            buttonUP,buttonDOWN,buttonRIGHT,buttonON,buttonMODE,
            buttonDEL,trigonometryButtonSection,buttonHYP,
            buttonYx,buttonExp,buttonSin,buttonSqrt,buttonAbc,
            buttonCos,buttonXsqr,buttonDms,buttonTan,buttonXcube,
            buttonRcl,buttonPI,buttonLog,buttonSto,buttonDrg,
            buttonIn,buttonMplus,bottomButtonSection,
            buttonDecimalPlace,
            buttonPlusMinus,buttonLeftBracket,
            buttonEquals,buttonRightBracket;

    private boolean error, lastNumber, lastDecimalPoint;
    private ImageView counter;
    private TextView tVScreen;
    private ListView list;
    private String[] calcMenu = {"Graphs","Matrices","Base Conversion","Decompression"};
    private static int bracketCounter = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tVScreen = findViewById(R.id.tVScreen);

        setNumericOnClickListener();
        setOperatorOnClickListener();

        list = (ListView)findViewById(R.id.listView);
        list.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, calcMenu));

        list.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id)
            {
                switch(pos){
                    case 0: Intent activityGraph = new Intent(getApplicationContext(), Graph.class);
                        startActivity(activityGraph);
                        break;
                    case 1: Intent activityMatrices = new Intent(getApplicationContext(), Matrices.class);
                        startActivity(activityMatrices);
                        break;
                    case 2: Intent activityBaseConversion = new Intent(getApplicationContext(), BaseConversion.class);
                        startActivity(activityBaseConversion);
                        break;
                    case 3: Intent activityCompression = new Intent(getApplicationContext(), CompressionActivity.class);
                        startActivity(activityCompression);
                        break;
                }
            }
        });
        // calculator case
        counter = findViewById(R.id.image);
        counter.setTranslationY(1000f);
        counter.setImageResource(R.drawable.calculatorcase);
    }

    public void calculatorCase(View view){
        counter.animate().translationYBy(-3000f).setDuration(1000);
    }

    private void setNumericOnClickListener()
    {
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Button buttonNumbers = (Button)v;
                if (error) {
                    tVScreen.setText(buttonNumbers.getText());
                    error = false;
                } else {
                    tVScreen.append(buttonNumbers.getText());
            }
                lastNumber = true;
            }
        };

        for (int id : buttonNumbers) {
            findViewById(id).setOnClickListener(listener);
        }
    }

    private void setOperatorOnClickListener()
    {

        View.OnClickListener listener = new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (!error && lastNumber)
                {
                    Button button = (Button) v;
                    tVScreen.append(button.getText());
                    lastDecimalPoint = false;
                    lastNumber = false;
                }
            }
        };

        for (int id : buttonOperators)
        {
            findViewById(id).setOnClickListener(listener);
        }

        findViewById(R.id.buttonEquals).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!error && lastNumber) {
                    // Read the expression
                    String txt = tVScreen.getText().toString();

                    // Create an Expression (A class from exp4j library)
                    //Expression expression = new ExpressionBuilder(txt).build();
                    try {

                        lastDecimalPoint = true; // Result contains a dot
                        Double doubleResult = ReversePolishNotation.run(txt, true);
                        String stringResult = doubleResult.toString();
                        // only here to show equals works
                        tVScreen.setText(stringResult);
                    } catch (ArithmeticException ex) {
                        tVScreen.setText("Error found");
                        error = true;
                        lastNumber = false;
                    }
                }
            }
        });

        findViewById(R.id.buttonDEL).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                StringBuilder tVScreenStrBuilder = new StringBuilder(tVScreen.getText());
                tVScreenStrBuilder.append(tVScreen);

                if(tVScreen.length() >= 1){
                    tVScreen.setText(tVScreenStrBuilder.substring(0, tVScreen.length()-1));
                }else{
                    return;
                }

                lastNumber = false;
                error = false;
                lastDecimalPoint = false;
            }
        });

        findViewById(R.id.buttonCos).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Button buttonNumbers = (Button)v;
                if (error) {
                    tVScreen.setText(buttonNumbers.getText());
                    error = false;
                } else {
                    tVScreen.append(buttonNumbers.getText());
                }
                lastNumber = true;
            }
        });

        findViewById(R.id.buttonLeftBracket).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Button button = (Button) v;
                String cos = button.getText().toString();

                tVScreen.append(cos.toLowerCase());
                bracketCounter++;

                lastNumber = false;
                error = false;
                lastDecimalPoint = false;
            }
        });

        findViewById(R.id.buttonRightBracket).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StringBuilder tVScreenStrBuilder = new StringBuilder(tVScreen.getText().toString());
                tVScreenStrBuilder.append(tVScreen);
                System.out.println("bracketCounter before --: " + bracketCounter);
                if(bracketCounter < 1){
                    System.out.println("bracketCounter = 0!!!: " + bracketCounter);
                    lastNumber = true;
                    return;
                }else{
                    Button button = (Button) v;
                    tVScreen.append(button.getText());

                    bracketCounter--;

                    System.out.println("bracketCounter after --: " + bracketCounter);

                    // // TODO: 03/10/2017  

                }
            }
        });
    }
}
