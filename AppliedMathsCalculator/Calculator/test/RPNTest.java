import static org.junit.Assert.*;

import org.junit.Test;

import com.calculator.RPN.ReversePolishNotation;

public class RPNTest {

	/*
	 * These tests perform simple two argument arithmitic
	 */
	@Test
	public void additionTestTwoArguments() {
		assertEquals(2, ReversePolishNotation.run("1+1", true), 0.001);
	}
	
	@Test
	public void subtractionTestTwoArguments() {
		assertEquals(1, ReversePolishNotation.run("2-1", true), 0.001);
	}
	
	@Test
	public void multiplicationTestTwoArguments() {
		assertEquals(6, ReversePolishNotation.run("3*2", true), 0.001);
	}
	
	@Test
	public void divisionTestTwoArguments() {
		assertEquals(3, ReversePolishNotation.run("6/2", true), 0.001);
	}
	
	/*
	 * These tests perform simple single argument trig arithmitic
	 * for both degrees and radians
	 */
	@Test
	public void cosTestRadians() {
		assertEquals(0.154, ReversePolishNotation.run("cos(30)", false), 0.001);
	}	
	@Test
	public void cosTestDegrees() {
		assertEquals(0.866, ReversePolishNotation.run("cos(30)", true), 0.001);
	}
	//need to fix
	@Test
	public void sinTestRadians() {
		assertEquals(-0.988, ReversePolishNotation.run("sin(30)", false), 0.001);
	}	
	@Test
	public void sinTestDegrees() {
		assertEquals(0.5, ReversePolishNotation.run("sin(30)", true), 0.001);
	}
	//need to fix -6.405
	@Test
	public void tanTestRadians() {
		assertEquals(-0.988, ReversePolishNotation.run("tan(30)", false), 0.001);
	}	
	@Test
	public void tanTestDegrees() {
		assertEquals(0.577, ReversePolishNotation.run("tan(30)", true), 0.001);
	}

}
