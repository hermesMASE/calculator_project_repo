package com.image.gaborWavelets;

import java.awt.image.BufferedImage;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author kfitzgerald
 */

public class Gabor
{
    CreateImage ci;
    BufferedImage returnedImage;
    private double lamda=0;
    private double theta=0;
    private double varphi=0;
    private double upsi=0;
    private double bandW=0;
    private double B=0;
    private double sigma=0;
    private double kappa=0;
    private double[][] GaborGrid;
    private double[][] GaborNorm;
    int counter = 0;
    int size=0;
    double toRadians=180/Math.PI, min=500, max=-500, mean=0;;
    int gaussian=0;
    double rotation;
    double GLFmean=0;
    
    //Standard Gabor no quantization
    public Gabor(double l, double t, double v, double u, double b, int sze, int counter){
    	this.counter = counter;
        lamda=l;
        theta=t/toRadians;
        varphi=v/toRadians;
        upsi=u;
        bandW=b;
        kappa=(2*Math.PI)/lamda;
        Calculate_Sigma();
        size=sze;
        gaussian=sze/2;
        GaborGrid = new double[size][size];
        GaborNorm = new double[size][size];
        RunGabor();
        //display(GaborGrid);
        NormaliseImage();
       // display(GaborNorm);
    }
    
    public void RunGabor(){
        /*Use the code provided along with your notes to code the Gabor
        Equation in to generate the correct image*/
    	double x = 0;
    	double y = 0;
    	double xA = 0;
    	double yA = 0;
    	int arrayX = 0;
    	int arrayY = 0;
    	double value = 0;
    	for(int i = -gaussian; i < gaussian; i++){
    		for(int j = -gaussian; j < gaussian; j++){
    			x = j;
        		y = i;
        		
        		xA = (x * Math.cos(theta)) + (y * Math.sin(theta));
        		yA = (-x * Math.sin(theta)) + (y * Math.cos(theta));
        		
        		value = Math.exp(-(Math.pow(xA, 2) + (Math.pow(yA, 2) * Math.pow(upsi, 2))) / (2 * Math.pow(sigma, 2)))
        				*
        				Math.cos(2 * Math.PI * (xA / lamda) + varphi);
        		GaborGrid[arrayX][arrayY] = value;
        		arrayY++;
        	}
    		arrayX++;
    		arrayY = 0;
    	} 	
    }
    
    public void display(double grid[][]){
    	for(int i = 0; i < size; i++){
    		for(int j = 0; j < size; j++){
    			System.out.print(grid[i][j]);
    		}
    		System.out.println();
    	}
    }
    
    
    public void NormaliseImage(){
    	double value = 0.0;
    	for(int i = 0; i < size; i++){
    		for(int j = 0; j < size; j++){
    			if(GaborGrid[i][j] < min){
    				min = GaborGrid[i][j];
    			}
    			if(GaborGrid[i][j] > max){
    				max = GaborGrid[i][j];
    			}
    		}
    	}
    	//System.out.println("min: " + min);
    	//System.out.println("max: " + max);
    	for(int i = 0; i < size; i++){
    		for(int j = 0; j < size; j++){
        		value = Math.ceil(((GaborGrid[i][j] - min) / (max - min)) * 255);
        		if(value < 0){
        			value = 0;
        		}
        		GaborNorm[i][j] = value;
        	}
    	}

        ci = new CreateImage(GaborNorm, "Gabor"+counter, counter);
        returnedImage = ci.getImage();
    }
    
    private void Calculate_Sigma(){
        B=(1/Math.PI)*(0.588705011)*((Math.pow(2, bandW)+1)/(Math.pow(2, bandW)-1));
        sigma=B*lamda;
    }
    
    public BufferedImage getImage(){
    	return returnedImage;
    }
}
