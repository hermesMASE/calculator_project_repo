/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author kfitzgerald
 */
package com.image.gaborWavelets;

import java.awt.image.BufferedImage;

public class Gabor_Driver
{
	static final int NUM_OF_PICS = 36;
	static BufferedImage[] images = new BufferedImage[NUM_OF_PICS + 1];
    public static void main(String[]args)
    {
        double lamda = 75;   //75
        double theta = 0;   //45;
        double varphi = 90;  //90
        double upsi = 1;  //1
        double bandW = 1; //1
        int size = 501;
        
        //loop creating images
        for(int i = 0; i < NUM_OF_PICS + 1; i++){
        	Gabor gabor = new Gabor(lamda, theta, varphi, upsi, bandW, size, i);
        	theta += (360 / NUM_OF_PICS);
        	images[i] = gabor.getImage();
        }
        //gabor .get buffered image and put into array
        //then put array into GifWriter run method
        try {
			GifSequenceWriter.Run(images);
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
}
