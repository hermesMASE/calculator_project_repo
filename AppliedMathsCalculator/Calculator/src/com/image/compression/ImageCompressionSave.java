package com.image.compression;

/* marta nanda 
 * jpg file compression into jpg file saved in the selected path
 * */

import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;
import javax.imageio.stream.MemoryCacheImageOutputStream;

public class ImageCompressionSave {

	public static void main(String[] args) {

		// This part needs to be fixed, to allow choose the path or
		// automatically save in the same path as original image
		File originalImage = new File(
				"C:\\Users\\marta\\Desktop\\plus na karte.jpg");
		File compressedImage = new File("C:\\Users\\marta\\Desktop\\plusC.jpg");
		try {
			// multiple choice of image compression for loop(?)
			compressJPEGImage(originalImage, compressedImage, 0.05f);
		} catch (IOException e) {

		}

	}

	public static void compressJPEGImage(File originalImage,
			File compressedImage, float compressionQuality) throws IOException {
		RenderedImage image = ImageIO.read(originalImage);
		// extend the image format to be in png ect.
		ImageWriter jpgWriter = ImageIO.getImageWritersByFormatName("jpg")
				.next();
		ImageWriteParam jpgWriterParam = jpgWriter.getDefaultWriteParam();
		jpgWriterParam.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
		jpgWriterParam.setCompressionQuality(compressionQuality);

		try (ImageOutputStream output = ImageIO
				.createImageOutputStream(compressedImage)) {
			jpgWriter.setOutput(output);
			IIOImage outputImage = new IIOImage(image, null, null);
			jpgWriter.write(null, outputImage, jpgWriterParam);
		}
		jpgWriter.dispose();
	}

}
