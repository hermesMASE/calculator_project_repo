package com.image.compression;


/*
 * @Vineeta Sharma
 * image of new size is send into DCT class so that it can be transposed in DCT class as height and width will be same
 * 
 */
import javax.imageio.ImageIO;
import javax.print.attribute.standard.Compression;

import com.image.greyscale.CreateImage;

import java.awt.image.BufferedImage;
import java.awt.image.MemoryImageSource;
import java.awt.image.PixelGrabber;
import java.io.*;
import java.util.Scanner;
import java.awt.*;
import java.awt.Image;
public class newImageCompression {
	static Image rgbImage = null;
	static Image gryImage = null;
	static int[] pixels;
	static int[] newPixels;
	static double[][] DArray;
	static int width;
	static int height;
	static int pixel;
	static Frame frame = new Frame();
	static File file = new File("C:\\\\Users\\\\A00248124\\\\Desktop\\\\math.jpg");
	static FileInputStream fis;
	static BufferedImage image;
	static int rows = 2;
	static int cols = 2;
	static int chunks = rows * cols;
	static int chunkWidth ;
	static int chunkHeight ;
	static int count = 0;
	static BufferedImage[] imgs;
	static BufferedImage resizedImage;

	public static void main(String[] args) throws IOException {
		try {
			Scanner sc = new Scanner(System.in);
			System.out.println("Enter Quality:");
			int Quality = sc.nextInt();
			fis = new FileInputStream(file);
			image = ImageIO.read(fis);			
			rgbImage = ImageIO.read(file);
			int widthold = rgbImage.getWidth(frame);
			int heightold = rgbImage.getHeight(frame);
			Dimension imgSize = new Dimension(widthold, heightold);
			Dimension boundary = new Dimension(150, 150);
			Dimension newDimension = getNewDimension(imgSize, boundary);
			width = (int) newDimension.getWidth();
			height =(int) newDimension.getHeight();
			DArray = new double[height][width];	
			// resized Image
			resizedImage = new BufferedImage(width, height, 4);
		    Graphics2D g = resizedImage.createGraphics();
		    g.drawImage(image, 0, 0, width, height, null);
		    try {
				ImageIO.write(resizedImage, "jpg", new File("C:\\Users\\A00248124\\Desktop\\Math_images\\newResizedImage.jpg"));
			} catch (Exception e) {
			}
		    g.dispose();
			
			grabPixels( Quality);			
			sc.close();
		} finally {
			System.out.println("in finally");
		}
	}

	public static void grabPixels(int Quality) {		
		PixelGrabber pixelGrabber = new PixelGrabber(resizedImage, 0, 0, width, height, true);
		System.out.println("width:" + width);
		System.out.println("height:" + height);
		System.out.println("size:" + height * width);
		int[] compressedImage = new int[height*width];
		double[][] temp1 = new double[height][width];
		int count = 0;				
		try {
			pixelGrabber.grabPixels();			
			System.out.println("grabbing Pixels:OK!");
		} catch (Exception e) {
			System.out.println("PixelGrabber exception");
		}
		pixels = ((int[]) pixelGrabber.getPixels());

		double[][] temp = new double[height][width];
		for (int y = 0; y < temp.length; y++) {
			for (int x = 0; x < temp[0].length; x++) {

				pixels[count] = convert2grey(pixels[count]);
				temp[y][x] = pixels[count];
				count++;
			}
		}

	//	compressedImage = DCT_Maths.runDCT(temp, height, width,Quality);
		/*
		 * gives error as Q Matrix in DCT_Maths class is 8x8 and we are doing compression for full image, Couldn't find any formula for calculating Q
		 * so, Unable to compress full image 
		 */
	//	Create(width, height, compressedImage, "compressed Image");
		new CreateImage(temp, "Greyscale");
	}

	public static void Create(int Width, int Height, int[] pixels, String n) {
		MemoryImageSource MemImg = new MemoryImageSource(Width, Height, pixels, 0, Width);
		Image img2 = Toolkit.getDefaultToolkit().createImage(MemImg);

		BufferedImage bfi = new BufferedImage(Width, Height, 4);
		Graphics2D g2D = bfi.createGraphics();

		g2D.drawImage(img2, 0, 0, height, width, null);
		try {
			ImageIO.write(bfi, "jpg", new File("C:\\Users\\A00248124\\Desktop\\Math_images\\imgcompress.jpg"));
		} catch (Exception e) {
		}
	}

	public static int convert2grey(int pixel) {
		int red = pixel >> 16 & 0xFF;
			int green = pixel >> 8 & 0xFF;
			int blue = pixel & 0xFF;
			return (int) (0.3D * red + 0.4D * green + 0.1D * blue);
	}

	public int getWidth() {
		return width;
	}


	public static Dimension getNewDimension(Dimension imgSize, Dimension boundary) {

		int original_width = imgSize.width;
		int original_height = imgSize.height;
		int bound_width = boundary.width;
		int bound_height = boundary.height;
		int new_width = original_width;
		int new_height = original_height;

		if (original_width > bound_width) {
			new_width = bound_width;
			new_height = bound_height;
		}

		return new Dimension(new_width, new_height);
	}
}

