package com.image.compression;

/**
 * Vineeta Sharma
 * Compression and Decompression of 8x8 matrix
 */

import java.text.DecimalFormat;
import java.util.Scanner;

public class ImageCompAndDecompMatrix_8x8 {

	static int[][] M =  {{16,8,23,16,5,14,7,22},{20,14,22,7,14,22,24,6},{15,23,24,23,9,6,6,20},{14,8,11,14,12,12,25,10},
			{10,9,11,9,13,19,5,17},{8,22,20,15,12,8,22,17},{24,22,17,12,18,11,23,14},{21,25,15,16,23,14,22,22}};
	static int N =8;
	static double n =8.0;
	static int[][] newM = new int[N][N];
	static double[][] T = new double[N][N];
	static double[][] Tt = new double[N][N];
	static double[][] tran = new double[N][N];
	static double[][] D = new double[N][N];
	static int[][] R = new int[N][N];
	static double[][] result = new double[N][N];
	static double[][] result1 = new double[N][N];
	static double[][] result2 = new double[N][N];
	static int[][] Q = {{16,11,10,16,24,40,51,61},{12,12,14,19,26,58,60,55},{14,13,16,24,40,57,69,56},{14,17,22,29,51,87,80,62},
			{18,22,37,56,68,109,103,77},{24,35,55,64,81,104,113,92},{49,64,78,87,103,121,120,101},{72,92,95,98,112,100,103,99}};
	static int[][] Q_result = new int[N][N];
	static int[][] C_result = new int[N][N];
	static double[][] C = new double[N][N];
	static DecimalFormat df = new DecimalFormat("####0.00");
	public static void main(String[] args){

		calculate_T();
		calculate_Tt();
		calculate_D();
		calculate_Q();
		calculate_C();
		calculate_R();
		calculate_newM();
		print_DCT();		
	}

	static void calculate_T(){
		for(int r=0;r<N;r++){
			for(int c=0;c<N;c++){
				if(r==0){
					T[r][c] = 1/ Math.sqrt(n);
				} else {
					T[r][c] = Math.sqrt(2.0/n)*Math.cos(((2*c+1)*(r*Math.PI))/(2*n));
				}
			}
		}
	}

	static void calculate_Tt(){
		for(int r=0;r<N;r++){
			for(int c=0;c<N;c++){
				Tt[c][r] = T[r][c];
			}
		}
	}

	static void calculate_D(){			
		double sum = 0.00;
		double sum1=0.00;
		tran = format_T(T);
		for(int r=0; r<N; r++)
		{
			for(int c=0; c<N; c++)
			{
				for(int k=0; k<N; k++)
								
					sum +=tran[r][k]*M[k][c];
				    
				result[r][c]=sum;
				sum=0.00;

			}
		}

		for(int r=0; r<N; r++)
		{
			for(int c=0; c<N; c++)
			{
				tran = format_T(Tt);
				for(int k=0; k<N; k++)									
					sum1+=result[r][k]*tran[k][c];
				    
				D[r][c]=sum1;
				sum1=0.00;
			}
		}
	}

	static double[][] format_T(double[][] array){
		double[][] newarray = new double[N][N];
		for(int i=0;i<N;i++){
			for(int j=0;j<N;j++){
				newarray[i][j] = (double) Math.round(array[i][j]*100)/100;
			}
		}
		return newarray;

	}

	static void calculate_Q(){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Quality:");
		int Qual = sc.nextInt();	
		for(int r=0;r<N;r++){
			for(int c=0;c<N;c++){
				if(Qual < 50) {
					Q_result[r][c] = (int)Math.round(Q[r][c]*(50/(double)Qual));	
					if(Q_result[r][c] >=255){
						Q_result[r][c]=255;
					}
				} else if( Qual == 50){
					Q_result[r][c] = Q[r][c];
				} else if(Qual > 50) {
					Q_result[r][c] = (int)Math.round(Q[r][c]*((100-(double)Qual)/50));
				}
			}
		}
	}

	static void calculate_C(){
		for(int r=0;r<N;r++){
			for(int c=0;c<N;c++){
				C_result[r][c]= (int)Math.round(D[r][c]/Q_result[r][c]);
			}
		}
	}

	static void calculate_R(){
		for(int r=0;r<N;r++){
			for(int c=0;c<N;c++){
				R[r][c] = Q_result[r][c]*C_result[r][c];
				
			}
		}
	}
	
	static void calculate_newM(){
		double sum = 0.00;
		double sum1=0.00;
		for(int r=0; r<N; r++)
		{
			for(int c=0; c<N; c++)
			{
				sum1=0.00;
				tran = format_T(Tt);
				for(int k=0; k<N; k++)								
					sum1+=tran[r][k]*R[k][c];				    
				result1[r][c]=sum1;

			}
		}

		for(int r=0; r<N; r++)
		{
			for(int c=0; c<N; c++)
			{
				tran = format_T(T);
				for(int k=0; k<N; k++)												
					result2[r][c] +=result1[r][k]*tran[k][c];				  
				newM[r][c] =(int) Math.round(result2[r][c]);

			}
		}

	}
	
	
	static void print_DCT(){
		System.out.println("Matrix M : ");
		for(int r=0;r<N;r++){
			for(int c=0;c<N;c++){
				System.out.print(M[r][c]+" ");
			}
			System.out.println();
		}
		System.out.println();
		System.out.println("Matrix T : ");
		for(int r=0;r<N;r++){
			for(int c=0;c<N;c++){
				System.out.print(df.format(T[r][c])+" ");
			}
			System.out.println();
		}
		System.out.println();
		System.out.println("Matrix Tt : ");
		for(int r=0;r<N;r++){
			for(int c=0;c<N;c++){
				System.out.print(df.format(Tt[r][c])+" ");
			}
			System.out.println();
		}
		System.out.println();
		System.out.println("Matrix tran : ");
		for(int r=0;r<N;r++){
			for(int c=0;c<N;c++){
				System.out.print(tran[r][c]+" ");
			}
			System.out.println();
		}
		System.out.println();
		System.out.println("Matrix result : ");
		for(int r=0;r<N;r++){
			for(int c=0;c<N;c++){
				System.out.print(df.format(result[r][c])+" ");
			}
			System.out.println();
		}
		System.out.println();
		System.out.println("Matrix D : ");
		for(int r=0;r<N;r++){
			for(int c=0;c<N;c++){
				System.out.print(df.format(D[r][c])+" ");
			}
			System.out.println();
		}
		System.out.println();
		System.out.println("Matrix Q : ");
		for(int r=0;r<N;r++){
			for(int c=0;c<N;c++){
				System.out.print(Q_result[r][c]+" ");
			}
			System.out.println();
		}
		System.out.println();
		System.out.println("Matrix C-old : ");
		for(int r=0;r<N;r++){
			for(int c=0;c<N;c++){
				System.out.print(C[r][c]+" ");
			}
			System.out.println();
		}
		System.out.println();
		System.out.println("Matrix C : ");
		for(int r=0;r<N;r++){
			for(int c=0;c<N;c++){
				System.out.print(C_result[r][c]+" ");
			}
			System.out.println();
		}
		System.out.println();
		System.out.println("Matrix R : ");
		for(int r=0;r<N;r++){
			for(int c=0;c<N;c++){
				System.out.print(R[r][c]+" ");
			}
			System.out.println();
		}
		System.out.println();
		System.out.println("Matrix result2 : ");
		for(int r=0;r<N;r++){
			for(int c=0;c<N;c++){
				System.out.print(result2[r][c]+" ");
			}
			System.out.println();
		}
		System.out.println();
		System.out.println("Matrix newM : ");
		for(int r=0;r<N;r++){
			for(int c=0;c<N;c++){
				System.out.print(newM[r][c]+" ");
			}
			System.out.println();
		}
	
	}
}
