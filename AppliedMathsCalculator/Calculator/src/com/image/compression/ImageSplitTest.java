package com.image.compression;

/* @Vineeta Sharma
 * generates mini images from full image for performing compression on each 8x8 block image by generating pixels
 * and sending each pixel array to DCT_Math for compression.
 * 
 * generates GreyScale image
 */
import javax.imageio.ImageIO;

import com.image.greyscale.CreateImage;

import java.awt.image.BufferedImage;
import java.awt.image.MemoryImageSource;
import java.awt.image.PixelGrabber;
import java.io.*;
import java.util.Scanner;
import java.awt.*;
import java.awt.Image;

public class ImageSplitTest {
	static Image rgbImage = null;
	static Image gryImage = null;
	static int[] pixels;
	static int[] newPixels;
	static double[][] DArray;
	static int width;
	static int height;
	static int pixel;
	static Frame frame = new Frame();
	static File file = new File("C:\\\\Users\\\\A00248124\\\\Desktop\\\\math.jpg");
	static FileInputStream fis;
	static BufferedImage image;
	static int rows = 19;
	static int cols = 40;
	static int chunks = rows * cols;
	static int chunkWidth ;
	static int chunkHeight ;
	static int count = 0;
	static BufferedImage[] imgs;

	public static void main(String[] args) throws IOException {
		
		try {
			Scanner sc = new Scanner(System.in);
			System.out.println("Enter Quality:");
			int Quality = sc.nextInt();
			fis = new FileInputStream(file);
			image = ImageIO.read(fis);
			chunkWidth = image.getWidth() / cols;
			chunkHeight = image.getHeight() / rows;
			rgbImage = ImageIO.read(file);
			width = rgbImage.getWidth(frame);
			height = rgbImage.getHeight(frame);
			DArray = new double[height][width];			
			imgs = new BufferedImage[chunks];
			for (int x = 0; x < rows; x++) {
				for (int y = 0; y < cols; y++) {

					imgs[count] = new BufferedImage(chunkWidth, chunkHeight, image.getType());
					Graphics2D gr = imgs[count++].createGraphics();
					gr.drawImage(image, 0, 0, chunkWidth, chunkHeight, chunkWidth * y, chunkHeight * x,
							chunkWidth * y + chunkWidth, chunkHeight * x + chunkHeight, null);
					gr.dispose();
				}
			}
			for (int i = 0; i < imgs.length; i++) {
				grabPixels(imgs[i],Quality); 
			}
			System.out.println("Splitting done");
		//	 writing mini images into image files
			for (int i = 0; i < imgs.length; i++) {
				ImageIO.write(imgs[i], "jpg", new File(
						"C:\\\\\\\\Users\\\\\\\\A00248124\\\\\\\\Desktop\\\\\\Math_images\\\\\\\\img" + i + ".jpg"));
			}
			System.out.println("Mini images created");
			sc.close();
		} finally {
			System.out.println("in finally");
		}
	}

	public static void grabPixels(BufferedImage imgs2,int Quality) {
//		PixelGrabber pixelGrabMiniImages = null;
		PixelGrabber pixelGrabber = new PixelGrabber(rgbImage, 0, 0, width, height, true);
		System.out.println("width:" + width);
		System.out.println("height:" + height);
		System.out.println("size:" + height * width);
//		int[] compressedImage = new int[height*width];
//		double[][] temp1 = new double[height][width];
		int count = 0;		
//		for (int i = 0; i < imgs2.BITMASK; i++) {
//			for(int y=0;y<temp1.length;y++){
//				for(int x=0;x<temp1[0].length;x++){
//					pixelGrabMiniImages = new PixelGrabber(imgs2, 0, 0, height, width, true); 
//					try {
//						pixelGrabMiniImages.grabPixels();
//					} catch (InterruptedException e1) {
//						e1.printStackTrace();
//					}
//					newPixels = (int[]) pixelGrabMiniImages.getPixels();
//					
//					temp1[y][x] = newPixels[count];
	//				compressedImage[i] = DCT_Maths.runDCT(temp1, height, width,Quality);  
					
					/* gives error as return type is int[] for runDCT method and it expects int, if return type is changed to int then
					 * we cannot store  all mini images in array to get them back as single compressed image
//					 */
//				}
//			}
//		}
		try {
			pixelGrabber.grabPixels();			
			System.out.println("grabbing Pixels:OK!");
		} catch (Exception e) {
			System.out.println("PixelGrabber exception");
		}
		pixels = ((int[]) pixelGrabber.getPixels());

		double[][] temp = new double[height][width];
		for (int y = 0; y < temp.length; y++) {
			for (int x = 0; x < temp[0].length; x++) {
				
				pixels[count] = convert2grey(pixels[count]);
				temp[y][x] = pixels[count];
				count++;
			}
		}
	//	compressedImage = DCT_Maths.runDCT(temp, height, width,Quality); 
		/* gives exception of array out of bound as while transposing image, it reverse height and width, 
		 * so width becomes height and height becomes width, but as height and width are of different sizes, it gives error.
		 */		
	//	Create(width, height, compressedImage, "compressed Image");
		new CreateImage(temp, "Greyscale");  // greyScale image
	}

//	public static void Create(int Width, int Height, int[] pixels, String n) {
//		MemoryImageSource MemImg = new MemoryImageSource(Width, Height, pixels, 0, Width);
//		Image img2 = Toolkit.getDefaultToolkit().createImage(MemImg);
//
//		BufferedImage bfi = new BufferedImage(Height, Width, 4);
//		Graphics2D g2D = bfi.createGraphics();
//
//		g2D.drawImage(img2, 0, 0, height, width, null);
//		try {
//			ImageIO.write(bfi, "jpg", new File("C:\\Users\\A00248124\\Desktop\\Math_images\\imgcompress.jpg"));
//		} catch (Exception e) {
//		}
//	}

	public static int convert2grey(int pixel) {
		int red = pixel >> 16 & 0xFF;
			int green = pixel >> 8 & 0xFF;
				int blue = pixel & 0xFF;
				return (int) (0.3D * red + 0.4D * green + 0.1D * blue);
	}

	public int getWidth() {
		return width;
	}
}