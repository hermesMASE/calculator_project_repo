package com.image.compression;
/*
 * Vineeta Sharma
 */
//import java.awt.Graphics2D;
//import java.awt.Image;
//import java.awt.Point;
//import java.awt.Toolkit;
//import java.awt.image.BufferedImage;
//import java.awt.image.MemoryImageSource;
//import java.awt.image.Raster;
//import java.awt.image.SampleModel;
//import java.awt.image.WritableRaster;
//import java.io.File;
import java.text.DecimalFormat;
//import java.util.Random;
//import java.util.Scanner;
//
//import javax.imageio.ImageIO;

public class DCT_Maths {

	static int N = 8;
	static double[][] data;
	static double n = 8.0;
	static double[][] T;
	static double[][] T1 ;
	static double[][] Tt ;
	static double[][] D ;
	static double[][] result ;
	static double[][] Q = { { 16, 11, 10, 16, 24, 40, 51, 61 }, { 12, 12, 14, 19, 26, 58, 60, 55 },
			{ 14, 13, 16, 24, 40, 57, 69, 56 }, { 14, 17, 22, 29, 51, 87, 80, 62 },
			{ 18, 22, 37, 56, 68, 109, 103, 77 }, { 24, 35, 55, 64, 81, 104, 113, 92 },
			{ 49, 64, 78, 87, 103, 121, 120, 101 }, { 72, 92, 95, 98, 112, 100, 103, 99 } };
	static double[][] Q_result ;
	static int[][] C_result ;
	static int[][] C ;
	static DecimalFormat df = new DecimalFormat("####0.00");

	public static int[] runDCT(double[][] dataFromImage, int height, int width,int Qual) {
		data = new double[height][width];
		T = new double[height][width];
		T1 = new double[height][width];
		Tt = new double[height][width];
		D = new double[height][width];
		result = new double[height][width];
		Q_result = new double[height][width];
		 C_result = new int[height][width];
		 C = new int[height][width];
		int[] newCompress = new int[height * width];
		for (int r = 0; r < height; r++) {
			for (int c = 0; c < width; c++) {
				data[r][c] = dataFromImage[r][c];
			}
		}
		calculate_T(height,  width);
		calculate_Tt( height,  width);
		calculate_D( height,  width);
		calculate_Q(Qual,height,width);
		int[][] compress = calculate_C(height,width);
		System.out.println("compress matrix : ");
		for(int r=0;r<height;r++){
			for(int c=0;c<width;c++){
				System.out.print(df.format(compress[r][c])+" ");
			}
			System.out.println();
		}
		System.out.println();
		for (int row = 0, count = 0; row < height; row++) {
			for (int col = 0; col < width; col++) {
				newCompress[count] = compress[row][col];
				count++;
			}
		}
		return newCompress;
	}

	static void calculate_T(int height, int width) {
		for (int r = 0; r < height; r++) {
			for (int c = 0; c < width; c++) {
				if (r == 0) {
					T1[r][c] = 1 / Math.sqrt(n);
					T = formatArray(T1);

				} else {

					T1[r][c] = Math.sqrt(2 / n) * Math.cos(((2 * c + 1) * (r * Math.PI)) / (2 * n));
					T = formatArray(T1);
				}
			}
		}
	}

	public static double[][] formatArray(double[][] array) {
		double[][] newArray = new double[array.length][array[0].length];
		for (int i = 0; i < array.length; i++) {
			for (int j = 0; j < array[0].length; j++) {
				newArray[i][j] = (double) Math.round(array[i][j] * 100.0) / 100.0;
			}
		}
		return newArray;
	}

	static void calculate_Tt(int height, int width) {
		for (int r = 0; r < height; r++) {
			for (int c = 0; c < width; c++) {
				Tt[c][r] = T[r][c];
			}
		}
	}

	static void calculate_D(int height, int width) {
		double sum1 = 0.00;
		for (int r = 0; r < height; r++) {
			for (int c = 0; c < width; c++) {
				for (int k = 0; k < width; k++) {
					result[r][c] = result[r][c] + T[r][k] * data[k][c];
				}
			}
		}

		for (int r = 0; r < height; r++) {
			for (int c = 0; c < width; c++) {
				sum1 = 0.00;
				for (int k = 0; k < width; k++) {
					sum1 = sum1 + result[r][k] * Tt[k][c];
				}
				D[r][c] = sum1;

			}
		}
	}

	static void calculate_Q(int Qual,int height, int width) {
		
		for (int r = 0; r < height; r++) {
			for (int c = 0; c < width; c++) {
				if (Qual < 50) {
					Q_result[r][c] = Q[r][c] * (50 / Qual);
				} else if (Qual > 50) {
					Q_result[r][c] = Q[r][c] * ((100 - Qual) / 50);
				} else if (Qual == 50) {
					Q_result[r][c] = Q[r][c];
				}
			}
		}

	}

	static int[][] calculate_C(int height, int width) {
		for (int r = 0; r < height; r++) {
			for (int c = 0; c < width; c++) {
				C[r][c] += (D[r][c] / Q[r][c]);
				C_result[r][c] = Math.round(C[r][c]);
			}
		}
		return C_result;
	}

}
