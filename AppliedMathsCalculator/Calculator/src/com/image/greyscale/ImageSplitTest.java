/**
 * @ Copyright 2017 ViniMukesh
 */
package com.image.greyscale;

import javax.imageio.ImageIO;

import com.image.greyscale.CreateImage;

import java.awt.image.BufferedImage;
import java.awt.image.PixelGrabber;
import java.io.*;
import java.awt.*;
import java.awt.Image;

public class ImageSplitTest {
	static Image rgbImage = null;
	static Image gryImage = null;
	static int[] pixels;
	static double[][] DArray;
	static int width;
	static int height;
	static int pixel;
	static Frame frame = new Frame();
	static File file = new File("C:\\\\Users\\\\marta\\\\Desktop\\\\buddhist-temple.jpg");
	public static void main(String[] args) throws IOException {
		try {
			FileInputStream fis = new FileInputStream(file);
			BufferedImage image = ImageIO.read(fis);
			BufferedImage greyImage = ImageIO.read(fis);
			 rgbImage = ImageIO.read(file);				
		width = rgbImage.getWidth(frame);
		height = rgbImage.getHeight(frame);
		DArray = new double[height][width];
		grabPixels();
		} finally {
			System.out.println("in finally");
		}
	}
	public static  void grabPixels() {
		PixelGrabber pixelGrabber = new PixelGrabber(rgbImage, 0, 0, width, height, true);
		System.out.println("width:" + width);
		System.out.println("height:" + height);
		System.out.println("size:" + height * width);

		try
		{
			pixelGrabber.grabPixels();
			System.out.println("grabbing Pixels:OK!");
		} catch (Exception e) {
			System.out.println("PixelGrabber exception"); }
		pixels = ((int[])pixelGrabber.getPixels());


		double[][] temp = new double[height][width];
		int count = 0;
		for (int y = 0; y < temp.length; y++)
		{
			for (int x = 0; x < temp[0].length; x++)
			{
				pixels[count] = convert2grey(pixels[count]);
				temp[y][x] = pixels[count];
				count++;
			}
		}
	
		 CreateImage ci = new CreateImage(temp, "Greyscale");
	}
	public static int convert2grey(int pixel) {
	    int red = pixel >> 16 & 0xFF;
	    int green = pixel >> 8 & 0xFF;
	    int blue = pixel & 0xFF;
	    return (int)(0.3D * red + 0.6D * green + 0.1D * blue);
	  }

	  public int getWidth() {
	    return width;
	  }
}
