package com.calculator.conversion;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Conversions {
	//all these converters assume the correct input values
	public String decimalToBinary(int number){
		String hex = "";
		ArrayList<Integer> bits = new ArrayList<Integer>();
		int remainder = 0;
		
		while(number != 0){
			remainder = number % 2;
			bits.add(remainder);
			number /= 2;			
		}
		Collections.reverse(bits);	
		for(int i: bits){
			hex += i;
		}
		return hex;
	}
	
	public int binaryToDecimal(String binary){
		int decimal = 0, power = 0, digit;
		for(int i = binary.length() - 1; i >= 0; i--){
			digit = Character.getNumericValue(binary.charAt(i));
			decimal += Math.pow(digit, power);
			power++;
		}
		return decimal;
	}
	
	public String binaryToHex(String input){
		String hexOutput = "";
		String leadingZeros = "";
		Boolean transferComplete = false;
		ArrayList<String > binarySplit = new ArrayList<String>();
		int stringIndex = input.length();
		while(!transferComplete){		
			if(stringIndex - 4 >= 0){
				binarySplit.add(input.substring(stringIndex - 4, stringIndex));
				stringIndex -= 4;
			}else{
				for(int i = stringIndex; i < 4; i++){
					leadingZeros += "0";
				}
				binarySplit.add(leadingZeros + input.substring(0, stringIndex));
				transferComplete = true;
			}
		}

		Collections.reverse(binarySplit);
		
		for(String s: binarySplit){
			switch(s){
				case "0000":
					hexOutput += "0";
					break;
				case "0001":
					hexOutput += "1";
					break;
				case "0010":
					hexOutput += "2";
					break;
				case "0011":
					hexOutput += "3";
					break;
				case "0100":
					hexOutput += "4";
					break;
				case "0101":
					hexOutput += "5";
					break;
				case "0110":
					hexOutput += "6";
					break;
				case "0111":
					hexOutput += "7";
					break;
				case "1000":
					hexOutput += "8";
					break;
				case "1001":
					hexOutput += "9";
					break;
				case "1010":
					hexOutput += "A";
					break;
				case "1011":
					hexOutput += "B";
					break;
				case "1100":
					hexOutput += "C";
					break;
				case "1101":
					hexOutput += "D";
					break;
				case "1110":
					hexOutput += "E";
					break;
				case "1111":
					hexOutput += "F";
					break;
			}
		}
		return hexOutput;
	}
	
	public String binaryToOctal(String input){
		String octalOutput = "";
		String leadingZeros = "";
		Boolean transferComplete = false;
		ArrayList<String > binarySplit = new ArrayList<String>();
		int stringIndex = input.length();
		while(!transferComplete){		
			if(stringIndex - 3 >= 0){
				binarySplit.add(input.substring(stringIndex - 3, stringIndex));
				stringIndex -= 3;
			}else{
				for(int i = stringIndex; i < 3; i++){
					leadingZeros += "0";
				}
				binarySplit.add(leadingZeros + input.substring(0, stringIndex));
				transferComplete = true;
			}
		}

		Collections.reverse(binarySplit);
		
		for(String s: binarySplit){
			switch(s){
				case "000":
					octalOutput += "0";
					break;
				case "001":
					octalOutput += "1";
					break;
				case "010":
					octalOutput += "2";
					break;
				case "011":
					octalOutput += "3";
					break;
				case "100":
					octalOutput += "4";
					break;
				case "101":
					octalOutput += "5";
					break;
				case "110":
					octalOutput += "6";
					break;
				case "111":
					octalOutput += "7";
					break;
			}
		}
		return octalOutput;
	}
	
	public String octalToBinary(String octal){
		String binaryResult = "";
		ArrayList<String > octalSplit = new ArrayList<String>(Arrays.asList(octal.split("")));
		for(String s : octalSplit){
			switch(s){
			case "0" :
				binaryResult += "000";
				break;
			case "1" :
				binaryResult += "001";
				break;
			case "2" :
				binaryResult += "010";
				break;
			case "3" :
				binaryResult += "011";
				break;
			case "4" :
				binaryResult += "100";
				break;
			case "5" :
				binaryResult += "101";
				break;
			case "6" :
				binaryResult += "110";
				break;
			case "7" :
				binaryResult += "111";
				break;
			}
		}
		return binaryResult;
	}
	
	public String decimalToHex(int decimal){
		String hex = "";
		hex = this.decimalToBinary(decimal);
		hex = this.binaryToHex(hex);
		return hex;
	}
	
	public String ipToBinary(String ip){
		String binary = "";
		int ipValue = 0;
		String [] ips = new String[4];
		ips = ip.split(".");
		System.out.println(ips.length);
		for(String s : ips){			
			ipValue = Integer.valueOf(s);
			binary += this.decimalToBinary(ipValue);
		}
		
		return binary;
	}
}

