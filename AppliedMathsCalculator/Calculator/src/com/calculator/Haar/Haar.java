package com.calculator.Haar;

/**
 * Vineeta Sharma
 * Compression and Decompression of 8x8 matrix using Haar
 */

public class Haar {
	private static int size = 8, N = 8;
	private static double[][] PixelArray = { { 230, 127, 127, 200, 200, 127, 127, 230 },
			{ 127, 230, 127, 230, 230, 127, 230, 127 }, { 127, 127, 230, 230, 230, 230, 127, 127 },
			{ 200, 230, 230, 255, 255, 230, 230, 200 }, { 200, 230, 230, 255, 255, 230, 230, 200 },
			{ 127, 127, 230, 230, 230, 230, 127, 127 }, { 127, 230, 127, 230, 230, 127, 230, 127 },
			{ 230, 127, 127, 200, 200, 127, 127, 230 } };
	private static double[][] tempArray = new double[N][N];

	static void decomposition() {
		int index = size;
		int step = 1;
		while (index >= 2) {
			int offset = index / 2;
			for (int k = 0; k < size; k++) {
				for (int j = 0; j < index; j += 2) {
					double avg = (PixelArray[k][j] + PixelArray[k][(j + 1)]) / 2.0D;
					double diff = PixelArray[k][j] - avg;

					tempArray[k][(j / 2)] = avg;
					tempArray[k][(offset + j / 2)] = diff;
				}
			}

			for (int k = 0; k < size; k++) {
				for (int j = 0; j < index; j++) {
					PixelArray[k][j] = tempArray[k][j];
				}
			}
			index /= 2;
			System.out.println("Rows decomposition step: " + step);
			for (int k = 0; k < size; k++) {
				for (int j = 0; j < size; j++) {
					System.out.print(PixelArray[k][j] + "\t");
				}
				System.out.println();
			}
			step++;
		}
		index = size;
		step = 1;
		while (index >= 2) {
			int offset = index / 2;

			for (int j = 0; j < size; j++) {
				for (int k = 0; k < index; k += 2) {
					double avg = (tempArray[k][j] + tempArray[(k + 1)][j]) / 2.0D;
					double diff = tempArray[k][j] - avg;

					PixelArray[(k / 2)][j] = avg;
					PixelArray[(offset + k / 2)][j] = diff;
				}
			}

			for (int j = 0; j < size; j++) {
				for (int k = 0; k < index; k++) {
					tempArray[k][j] = PixelArray[k][j];
				}
			}
			index /= 2;
			System.out.println("columns decomposition step: " + step);
			for (int k = 0; k < size; k++) {
				for (int j = 0; j < size; j++) {
					System.out.print(PixelArray[k][j] + "\t");
				}
				System.out.println();
			}
			step++;
		}
		System.out.println("final decom");
		for (int k = 0; k < size; k++) {
			for (int j = 0; j < size; j++) {
				System.out.print(PixelArray[k][j] + "\t");
			}
			System.out.println();
		}
	}

	static void recomposition() {
		int index = 1;
		int step = 1;
		while (index < size) {
			for (int i = 0; i < size; i++) {
				for (int j = 0; j < index; j++) {
					tempArray[i][(2 * j)] = (PixelArray[i][j] + PixelArray[i][(index + j)]);
					tempArray[i][(2 * j + 1)] = (PixelArray[i][j] - PixelArray[i][(index + j)]);
				}
			}

			index *= 2;
			for (int k = 0; k < size; k++) {
				for (int j = 0; j < index; j++) {
					PixelArray[k][j] = tempArray[k][j];
				}
			}
			System.out.println("columns recomposition step: " + step);
			for (int k = 0; k < size; k++) {
				for (int j = 0; j < size; j++) {
					System.out.print(PixelArray[k][j] + "\t");
				}
				System.out.println();
			}
			step++;
		}

		index = 1;
		step = 1;

		while (index < size) {
			for (int j = 0; j < size; j++) {
				for (int i = 0; i < index; i++) {
					PixelArray[(2 * i)][j] = (tempArray[i][j] + tempArray[(index + i)][j]);
					PixelArray[(2 * i + 1)][j] = (tempArray[i][j] - tempArray[(index + i)][j]);
				}
			}

			index *= 2;
			for (int j = 0; j < size; j++) {
				for (int k = 0; k < index; k++) {
					tempArray[k][j] = PixelArray[k][j];
				}
			}
			System.out.println("rows recomposition step: " + step);
			for (int k = 0; k < size; k++) {
				for (int j = 0; j < size; j++) {
					System.out.print(PixelArray[k][j] + "\t");
				}
				System.out.println();
			}
			step++;
		}
		System.out.println("final recomposition");
		for (int k = 0; k < size; k++) {
			for (int j = 0; j < size; j++) {
				System.out.print(PixelArray[k][j] + "\t");
			}
			System.out.println();
		}
	}

	public static void main(String[] args) {
		System.out.println("decomposed matrix:");
		decomposition();
		System.out.println("recomposed matrix");
		recomposition();
	}
}
