package com.calculator.PlotFunction;

import javax.swing.JFrame;
import java.util.Scanner;

public class PlotFunction {
	//TESTING TESTING
	static Scanner input = new Scanner(System.in);
	
	public static void main(String[] args){
		System.out.print("Enter size of frame: ");
		int size = Integer.parseInt(input.nextLine());
		System.out.print("Enter maximum value for x: ");
		double value = input.nextDouble();
		input.nextLine();
		JFrame frame = new JFrame("Plot Function");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		PlotFunctionPanel panel = new PlotFunctionPanel(size, value);
		frame.getContentPane().add(panel);
		frame.pack();
		frame.setVisible(true);
	}
}
