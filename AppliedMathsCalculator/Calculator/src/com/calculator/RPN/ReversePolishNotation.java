package com.calculator.RPN;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Stack;
 /*
  * http://andreinc.net/2010/10/05/converting-infix-to-rpn-shunting-yard-algorithm/
  * all the code from the * line i wrote
  */
public class ReversePolishNotation {
	// Associativity constants for operators
	private static final int LEFT_ASSOC = 0;
	private static final int RIGHT_ASSOC = 1;
	
	
	private static final Double EXPONENTIAL_CONSTANT = 2.71828d; 
	private static final Double PI_CONSTANT = Math.PI; 

	// Supported operators
	private static final Map<String, int[]> OPERATORS = new HashMap<String, int[]>();
	
	private static boolean negativeHit = false;
	
	static {
		// Map<"token", []{precendence, associativity}>
		OPERATORS.put("+", new int[] { 0, LEFT_ASSOC });
		OPERATORS.put("-", new int[] { 0, LEFT_ASSOC });
		OPERATORS.put("*", new int[] { 5, LEFT_ASSOC });
		OPERATORS.put("/", new int[] { 5, LEFT_ASSOC });
		OPERATORS.put("%", new int[] { 5, LEFT_ASSOC });
		OPERATORS.put("^", new int[] { 10, RIGHT_ASSOC });
	}

	/**
	 * Test if a certain is an operator .
	 * @param token The token to be tested .
	 * @return True if token is an operator . Otherwise False .
	 */
	private static boolean isOperator(String token) {
		return OPERATORS.containsKey(token);
	}

	/**
	 * Test the associativity of a certain operator token .
	 * @param token The token to be tested (needs to operator).
	 * @param type LEFT_ASSOC or RIGHT_ASSOC
	 * @return True if the tokenType equals the input parameter type .
	 */
	private static boolean isAssociative(String token, int type) {
		if (!isOperator(token)) {
			throw new IllegalArgumentException("Invalid token: " + token);
		}
		if (OPERATORS.get(token)[1] == type) {
			return true;
		}
		return false;
	}

	/**
	 * Compare precendece of two operators.
	 * @param token1 The first operator .
	 * @param token2 The second operator .
	 * @return A negative number if token1 has a smaller precedence than token2,
	 * 0 if the precendences of the two tokens are equal, a positive number
	 * otherwise.
	 */
	private static final int cmpPrecedence(String token1, String token2) {
		if (!isOperator(token1) || !isOperator(token2)) {
			throw new IllegalArgumentException("Invalied tokens: " + token1
					+ " " + token2);
		}
		return OPERATORS.get(token1)[0] - OPERATORS.get(token2)[0];
	}

	public static String[] infixToRPN(String[] inputTokens) {
		ArrayList<String> out = new ArrayList<String>();
		Stack<String> stack = new Stack<String>();
		// For all the input tokens [S1] read the next token [S2]
		for (String token : inputTokens) {
			if (isOperator(token)) {
				// If token is an operator (x) [S3]
				while (!stack.empty() && isOperator(stack.peek())) {
					// [S4]
					if ((isAssociative(token, LEFT_ASSOC) && cmpPrecedence(
							token, stack.peek()) <= 0)
							|| (isAssociative(token, RIGHT_ASSOC) && cmpPrecedence(
									token, stack.peek()) < 0)) {
						out.add(stack.pop()); 	// [S5] [S6]
						continue;
					}
					break;
				}
				// Push the new operator on the stack [S7]
				stack.push(token);
			} else if (token.equals("(")) {
				stack.push(token); 	// [S8]
			} else if (token.equals(")")) {
				// [S9]
				while (!stack.empty() && !stack.peek().equals("(")) {
					out.add(stack.pop()); // [S10]
				}
				stack.pop(); // [S11]
			} else {
				out.add(token); // [S12]
			}
		}
		while (!stack.empty()) {
			out.add(stack.pop()); // [S13]
		}
		String[] output = new String[out.size()];
		return out.toArray(output);
	}
	
	
//*********************************************************************************
	public static double run(String userInput, Boolean radDeg) {
//
//		System.out.print("Enter expression to convert: ");
//		//taking in expression from user
//		String inputText = sc.nextLine();
		
		//converting input into an array of Strings and being validated for other methods (adding spaces) -> needed for infoxToRPN method 
		String [] input = inputFormatter(userInput, radDeg);
		//printing out validated input
//		System.out.print("Validation input: ");
//		for (String token : input) {
//			System.out.print(token);
//		}
		System.out.println();
		//the output without formating
		//String[] output = outputFormatter(infixToRPN(input));
		String[] output = infixToRPN(input);		
//		System.out.print("Output Expression: ");
//		printing the outputed array of 1 character Strings
//		for (String token : output) {
//			System.out.print(token);
//		}
		
		
//		//**************JUST FOR SHOW ***********************
		System.out.println();
//		//printing out the formatted output
//		String[] outputFormatted = outputFormatter(infixToRPN(input));
//		System.out.print("Output Expression Formatted: ");
//		//printing the outputed array of 1 character Strings
//		for (String token : outputFormatted) {
//			System.out.print(token);
//		}
//		System.out.println();
//		//**************JUST FOR SHOW ***********************
		
		
		//removing spaces so the evaluator will work
//		output = spaceRemover(output);
//		System.out.println("Values to be passed into the evaluator: ");
//		for (String token : output) {
//			System.out.print(token);
//		}
		System.out.println();
		//System.out.println("Spaces removed: " + spaceRemover(output));
		//System.out.println("Result of Evaluator: " + evaluator(output));
		return evaluator(output);
	}

	//this method formats the input from the user into the desired format and also validates the input e.g no letters
	
	//could use &'s to designate when a multi-digit number is present
	
	public static String[] inputFormatter(String input, Boolean radDeg){		
		boolean spaceHit = false;
		boolean validEntry = true;
		char ch;
		
		String expressionString ="";
		boolean endOfExpression = false;
		int openingBracketCount = 0;
		
		boolean trigExpressionTrigger = false;
		boolean logExpressionTrigger = false;
		boolean rootExpressionTrigger = false;
		boolean exponentExpressionTrigger = false;
		boolean factorialExpressionTrigger = false;
		//use this to determine if &should be added to numbers in trig and log functions
		
		String [] returnArray;

		//arrayList of the characters in the input array copied
		ArrayList<String> characterList = new ArrayList<>(Arrays.asList(input.split("")));	
		//list that will be converted to an array and returned, chose to do this because it was easier to add things and don't have to worry about the size
		ArrayList<String> returnList = new ArrayList<>();
		//1 error  on (null) value from old list conversion

//I added in this loop to clean up the list and remove any null values that may be coming in
		for(int i = 0; i < characterList.size(); i++){
			if(characterList.get(i) == null ){
				characterList.remove(i);
			}
		}
		
		boolean endOfDigits = false;			
		for(int i = 0; i < characterList.size() && validEntry; i++){  //changed from 0 to 1 was working at 0
			//chose to use chars because its easier to validate them than strings imo
			ch = characterList.get(i).charAt(0);
				
			if(Character.isAlphabetic(ch) || trigExpressionTrigger 
			   || logExpressionTrigger || rootExpressionTrigger || factorialExpressionTrigger){
				//in here for cos tan sin Acos Atan Asin 
				if(trigExpressionTrigger || (ch == 'c' || ch == 't' || ch == 's' || ch == 'A')){
					trigExpressionTrigger = true;
					//trigExpressionString += Character.toString(ch);
					while(!endOfExpression){
						ch = characterList.get(i).charAt(0);
						if(ch != '(' && ch != ')'){
							expressionString  += Character.toString(ch);
							i++;
						}else if(ch == '('){ 
							expressionString  += Character.toString(ch);					
							openingBracketCount++;
							i++;
						}else if(ch == ')'){  //taking advantage of the prefix decrement
							if(--openingBracketCount == 0){
								endOfExpression = true;
								expressionString  += Character.toString(ch);
								//need to take in boolean from user to decide degrees or radians
								returnList.add(trigEvaluator(expressionString, radDeg));
								expressionString= "";
								trigExpressionTrigger = false;
								i++;
							}else{
								expressionString  += Character.toString(ch);
								i++;
							}						
						}
					}
				}else if(logExpressionTrigger || ch == 'l' ){  //log and ln
					logExpressionTrigger = true;
					while(!endOfExpression){
						ch = characterList.get(i).charAt(0);
						if(ch != '(' && ch != ')'){
							expressionString  += Character.toString(ch);
							i++;
						}else if(ch == '('){ 
							expressionString  += Character.toString(ch);					
							openingBracketCount++;
							i++;
						}else if(ch == ')'){  //taking advantage of the prefix decrement
							if(--openingBracketCount == 0){
								endOfExpression = true;
								expressionString  += Character.toString(ch);
								//need to take in boolean from user to decide degrees or radians
								returnList.add(logEvaluator(expressionString));
								expressionString= "";
								logExpressionTrigger = false;
								i++;
							}else{
								expressionString  += Character.toString(ch);
								i++;
							}						
						}
					}
				}else if(rootExpressionTrigger || ch == 'q'){
					rootExpressionTrigger = true;
					while(!endOfExpression){
						ch = characterList.get(i).charAt(0);
						if(ch != '(' && ch != ')'){
							expressionString  += Character.toString(ch);
							i++;
						}else if(ch == '('){ 
							expressionString  += Character.toString(ch);					
							openingBracketCount++;
							i++;
						}else if(ch == ')'){  //taking advantage of the prefix decrement
							if(--openingBracketCount == 0){
								endOfExpression = true;
								expressionString  += Character.toString(ch);
								//need to take in boolean from user to decide degrees or radians
								returnList.add(rootEvaluator(expressionString));
								expressionString= "";
								rootExpressionTrigger = false;
								i++;
							}else{
								expressionString  += Character.toString(ch);
								i++;
							}						
						}
					}
					
				}else if(exponentExpressionTrigger || (ch == 'e' || ch == 'E')){
					returnList.add("&" + EXPONENTIAL_CONSTANT.toString()  + "£");
				}else if(factorialExpressionTrigger || ch == 'f'){
					factorialExpressionTrigger = true;
					while(!endOfExpression){
						ch = characterList.get(i).charAt(0);
						if(ch != '(' && ch != ')'){
							expressionString  += Character.toString(ch);
							i++;
						}else if(ch == '('){ 
							expressionString  += Character.toString(ch);					
							openingBracketCount++;
							i++;
						}else if(ch == ')'){  //taking advantage of the prefix decrement
							if(--openingBracketCount == 0){
								endOfExpression = true;								
								expressionString  += Character.toString(ch);
								//need to take in boolean from user to decide degrees or radians
								returnList.add(factorialEvaluator(expressionString));
								expressionString= "";
								factorialExpressionTrigger = false;
								i++;
							}else{
								expressionString += Character.toString(ch);
								i++;
							}						
						}
					}
				}
			}else if(Character.isDigit(ch) || ch == '!'){	
				//need to sort out adding numbers to the list				
					returnList.add("&");
					returnList.add(Character.toString(ch));
					if(ch == '!'){
						negativeHit = true;
					}					
					for(int j = i + 1; j < characterList.size() && !endOfDigits; j++){
						ch = characterList.get(j).charAt(0);
						if(!Character.isDigit(ch) && ch != '.'){
							endOfDigits = true;	
							
						}else{
							returnList.add(Character.toString(ch));
							i++;
						}
					}
					endOfDigits = false;
			}else{
				//always adding the first symbol in the input straight onto the out put list as there is no need to check if there is a space before it
				if(i == 0){
					returnList.add(characterList.get(i));
					
				}else{
					//checking if the current value is not a space and we have not hit a space so we need to add a space to the list before we add the character
					if(ch != ' ' && !spaceHit){
						returnList.add(" ");
						returnList.add(characterList.get(i));
					}
					//checking that the current value is not a space and we have already come across a space so we dont have to add a space before we add the value to the list
					else if(ch != ' ' && spaceHit){
						returnList.add(characterList.get(i));
						spaceHit = false;
					}
					//checking if the current value is a space so we know we have hit a space and to add a space to the list
					else if(ch == ' '){
						spaceHit = true;
						returnList.add(" ");
					}
				}
			}

		}
		//setting the length of the array to be returned equal to the length of the list we just populated
		returnArray = new String[returnList.size()];
		//checking that the expression is valid before returning an invalid array
		if(validEntry){	
			//copying the contents of the array list into the array
			returnArray = returnList.toArray(returnArray);
			//returning the array
			return returnArray;
		}else{
			//printing out a validation error to the console
			System.out.println("Incorrect entry detected...letter found in mathematical expression");
			//returning null so that an improper list is not handed down and the program will CRASH!!!!!!
			return null;
		}
	}
	
	private static String rootEvaluator(String input){
		String resultString = "";
		String rootDegree = input.substring(3, 4);
		double rootDegreeValue = Double.parseDouble(rootDegree);
		String insideExpression = input.substring(input.indexOf('(') + 1, input.lastIndexOf(')'));
		//adding £ symbol so peek() wont cause a crash when it retuns null -- caused by the last element in the formatted list being a number
		insideExpression += "£";
		double insideExpressionValue = run(insideExpression, true);
		Double rootResult = 0.0d;
		

		//https://stackoverflow.com/questions/32553108/calculating-nth-root-in-java-using-power-method
		rootResult = Math.pow(insideExpressionValue, (1 / rootDegreeValue));
			
		if(rootResult < 0){
			if(negativeHit){
				rootResult = Math.abs(rootResult);
				resultString = rootResult.toString();
			}else{
				rootResult = Math.abs(rootResult);
				resultString = "&!" + rootResult.toString() + "£";
			}
		}else{
			if(negativeHit){
				rootResult = Math.abs(rootResult);
				resultString = rootResult.toString() + "£";
			}else{
				resultString = "&" + rootResult.toString() + "£";
			}
		}
		return resultString;
	}
	
	//method that evaluate trignometric functions
	private static String trigEvaluator(String input, Boolean degrees){
		String resultString = "";
		String insideExpression = input.substring(input.indexOf('(') + 1, input.lastIndexOf(')'));
		//adding £ symbol so peek() wont cause a crash when it retuns null -- caused by the last element in the formatted list being a number
		insideExpression += "£";
		double insideExpressionValue = run(insideExpression, degrees);
		Boolean illegalArcInput = (insideExpressionValue > -1 && insideExpressionValue < 1) ? false : true;
		
		Double trigResult = 0.0d;
		String trigType;
		
		if(input.charAt(0) == 'A'){
			trigType = input.substring(0, 4);
		}else{
			trigType = input.substring(0, 3);
		}

		switch(trigType){
			case "cos":
				if(degrees){
					if(insideExpressionValue % 180 != 0 && insideExpressionValue % 90 == 0){
						trigResult = 0d;
					}else{
						trigResult = Math.cos(Math.toRadians(insideExpressionValue));
					}
				}else{
					if(Math.toDegrees(insideExpressionValue) == 90d){
						trigResult = 0d;
					}else{
						trigResult = Math.cos(insideExpressionValue);
					}
				}				
				break;
			case "sin":
				if(degrees){
					if(insideExpressionValue % 180 == 0){
						trigResult = 0d;
					}else{
						trigResult = Math.sin(Math.toRadians(insideExpressionValue));
					}
				}else{
					if(insideExpressionValue % PI_CONSTANT == 0){
						trigResult = 0d;
					}else{
						trigResult = Math.sin(insideExpressionValue);
					}					
				}
				
				break;
			case "tan":
				if(degrees){
					if(insideExpressionValue % 180 == 0){
						trigResult = 0d;
					}else if(insideExpressionValue % 90 == 0){
						//INFINITY
						throw new IllegalArgumentException();
					}else{
						trigResult = Math.tan(Math.toRadians(insideExpressionValue));
					}
				}else{
					if(insideExpressionValue % PI_CONSTANT == 0){
						trigResult = 0d;
					}else if(insideExpressionValue % (PI_CONSTANT / 2)== 0){
						//INFINITY
						throw new IllegalArgumentException();
					}else{
						trigResult = Math.tan(insideExpressionValue);
					}
				}
				break;
				
			case "Acos":
				if(illegalArcInput){
					throw new IllegalArgumentException();
				}else{
					if(degrees){
						trigResult = Math.toDegrees(Math.acos(insideExpressionValue));
					}else{
						trigResult = Math.acos(insideExpressionValue);
					}
				}
				break;
			case "Asin":
				if(illegalArcInput){
					throw new IllegalArgumentException();
				}else{
					if(degrees){
						trigResult = Math.toDegrees(Math.asin(insideExpressionValue));
					}else{
						trigResult = Math.asin(insideExpressionValue);
					}
				}
				
				break;
			case "Atan":	
				if(degrees){
					trigResult = Math.toDegrees(Math.asin(insideExpressionValue));
				}else{
					trigResult = Math.atan(insideExpressionValue);
				}
				break;
		}
		//need to test if positive or negative 
		if(trigResult < 0){
			if(negativeHit){
				trigResult = Math.abs(trigResult);
				resultString = trigResult.toString();
			}else{
				trigResult = Math.abs(trigResult);
				resultString = "&!" + trigResult.toString() + "£";
			}
		}else{
			if(negativeHit){
				trigResult = Math.abs(trigResult);
				resultString = trigResult.toString() + "£";
			}else{
				resultString = "&" + trigResult.toString() + "£";
			}
		}
		return resultString;
	}
	
	private static String logEvaluator(String input){
		String resultString = "";
		String logBase = "";
		Double logBaseValue = 0.0d;
		Double logResult = 0.0d;
		String insideExpression = input.substring(input.indexOf('(') + 1, input.lastIndexOf(')'));
		//adding £ symbol so peek() wont cause a crash when it retuns null -- caused by the last element in the formatted list being a number
		insideExpression += "£";

		//just set to true but it doesnt matter here
		double insideExpressionValue = run(insideExpression, true);
		String logType = "";
		//if an in is the second character then we know we have ln
		if(input.charAt(1) == 'n'){
			logType = input.substring(0, 2); 
		}else{
			logType = input.substring(0, 3);
			logBase = input.substring(3, input.indexOf('('));
			logBaseValue = Double.parseDouble(logBase);
		}

		switch(logType){
			case "ln" :
				logResult = Math.log(insideExpressionValue);
				break;
			case "log" :
				logResult = Math.log(insideExpressionValue) / Math.log(logBaseValue);
				break;
		}

		if(logResult < 0){
			if(negativeHit){
				logResult = Math.abs(logResult);
				resultString = logResult.toString();
			}else{
				logResult = Math.abs(logResult);
				resultString = "&!" + logResult.toString() + "£";
			}
		}else{
			if(negativeHit){
				logResult = Math.abs(logResult);
				resultString = logResult.toString() + "£";
			}else{
				resultString = "&" + logResult.toString() + "£";
			}
		}
		return resultString;
	}
	
	//this method just formats the output array from the validate and infoxToRPN methods so that there are no & 
	//!!probably a way better way of doing this but it works, using .replace wasn't working for some reason when i tried
	//this method is just so it looks easier to see and is not actually used in the evaluation
//	public static String[] outputFormatter(String[] input){
//		//array that is going to be returned
//		String[] returnArray;
//		//string the array will be converted to and from
//		String inputAsString = "";
//		//loop to go through the array and only add non & strings
//		for(String s : input){
//			if(!s.equals("&")){
//				inputAsString += s;
//			}			
//		}
//		//splitting up to string back into the array but with no spaces present
//		returnArray = inputAsString.split("");
//		//returning the cleaned array
//		return returnArray;
//	}
	
	//this method id used to remove the spaces in the array so the evaluator wont come across any spaces
//	public static String[] spaceRemover(String[] input){
//		//array that is going to be returned
//		String[] returnArray;
//		//string the array will be converted to and from
//		String inputAsString = "";
//		//loop to go through the array and only add non space strings
//		for(String s : input){
//			if(!s.equals(" ")){
//				inputAsString += s;
//			}			
//		}
//		//splitting up to string back into the array but with no spaces present
//		returnArray = inputAsString.split("");
//		//returning the cleaned array
//		return returnArray;
//	}
	private static String factorialEvaluator(String input){
		String resultString = "";
		String insideExpression = input.substring(input.indexOf('(') + 1, input.lastIndexOf(')'));
		insideExpression += "£";
		Double factorialValue = 0.0;
		double insideExpressionValue = run(insideExpression, true);
		
		if(insideExpressionValue < 0 || insideExpressionValue % 1.0 != 0){
			throw new IllegalArgumentException();
		}else{
			factorialValue = insideExpressionValue;
			for(double i = insideExpressionValue - 1; i > 0; i--){
				factorialValue *= i; 
			}
		}
		if(negativeHit){
			factorialValue = Math.abs(factorialValue);
			resultString = factorialValue.toString() + "£";
		}else{
			resultString = "&" + factorialValue.toString() + "£";
		}
		return resultString;
		
	}
	
	private static double operandStateChecker(String input){
		double returnValue = 0.0;
		String numberToReturn = "";
		if(input.indexOf('!') == -1){
			numberToReturn = input;
			returnValue = Double.parseDouble(numberToReturn);
		}else{
			numberToReturn = input.substring(1);
			returnValue = Double.parseDouble(numberToReturn);
			returnValue *= -1;
		}
		return returnValue;
	}
	
	
	private static double evaluator(String [] input){
		double finalResult = 0;
		double leftSide = 0d;
		double rightSide = 0d;
		double result = 0d;
		String numberBuilder = "";
		String testString = "";
		LinkedList<String> listInput  = new LinkedList<>();
		LinkedList<String> workingList = new LinkedList<>();
		//ArrayList<String> workingList = new LinkedList<>();
		String tempArray[];
		//boolean singleOperand = false;
		int numberCount = 0;
		//if there is only one value in the input e.g a single value split it into an array 
		//-- this happens when a value is being returned from the trig evaluator or the log evaluator
		
		//need to check if a bit of the array has more than one character and then expand it
		if(input.length == 1){
			tempArray = input[0].split("");
			listInput =  new LinkedList<>(Arrays.asList(tempArray));
		}else{
			for(String s : input){
				if(s.length() > 1){
					tempArray = s.split("");
					for(String t : tempArray){
						listInput.add(t);
					}					
				}else{
					listInput.add(s);
				}
			}
		}
		
		for(String s : listInput){
			if(s.equals("&")){
				numberCount++;
			}
		}
		
		while(listInput.size() > 0 && numberCount > 1){
			testString = listInput.pop();
			//test if value is the start of a digit
			if(testString.equals("&")){
				//write a method to replace this			
				//need to change this so its a boolean and do the checks inside the while and prepare for a null
				while(Character.isDigit(listInput.peek().charAt(0)) || listInput.peek().charAt(0) == '.' || listInput.peek().charAt(0) == '!'){
					testString = listInput.pop();
					numberBuilder += testString;
				}
				workingList.push(numberBuilder);
				numberBuilder = "";
			}else if(isOperator(testString)){			
				switch(testString){
					case "+":
//						rightSide = Double.parseDouble(workingList.pop());
//						leftSide = Double.parseDouble(workingList.pop());
						rightSide = operandStateChecker(workingList.pop());
						leftSide = operandStateChecker(workingList.pop());
						result = leftSide + rightSide;					
						workingList.push(Double.toString(result));
						break;
					case "-":
						rightSide = operandStateChecker(workingList.pop());
						leftSide = operandStateChecker(workingList.pop());
						result = leftSide - rightSide;					
						workingList.push(Double.toString(result));						
						break;
					case "*":
						rightSide = operandStateChecker(workingList.pop());
						leftSide = operandStateChecker(workingList.pop());
						result = leftSide * rightSide;					
						workingList.push(Double.toString(result));
						break;
					case "/":
						rightSide = operandStateChecker(workingList.pop());
						leftSide = operandStateChecker(workingList.pop());
						if(rightSide == 0){
							//workingList.push("0");
							throw new IllegalArgumentException();
						}else if(leftSide == 0){
							workingList.push("0");
						}else{
							result = leftSide / rightSide;					
							workingList.push(Double.toString(result));
						}
						break;
					case "%":
						rightSide = operandStateChecker(workingList.pop());
						leftSide = operandStateChecker(workingList.pop());
						result = leftSide % rightSide;					
						workingList.push(Double.toString(result));
						break;
					case "^":
						rightSide = operandStateChecker(workingList.pop());
						leftSide = operandStateChecker(workingList.pop());
						result = Math.pow(leftSide, rightSide);					
						workingList.push(Double.toString(result));
						break;
				}
			}
		}
		//System.out.println("!!"+workingList.peek());
		if(numberCount > 1){
			finalResult = Double.parseDouble(workingList.pop());   //this one works
		}else{
			//need to fix thus while as it is looking at an array not a list
			listInput.pop(); //removing & from the list
			while(Character.isDigit(listInput.peek().charAt(0)) || listInput.peek().charAt(0) == '.' || listInput.peek().charAt(0) == '!'){
				testString = listInput.pop();
				numberBuilder += testString;
			}			
			finalResult = operandStateChecker(numberBuilder);
			numberBuilder = "";		
		}
		//finalResult = operandStateChecker(workingList.pop());  //this is a test
		
		return finalResult;
	}
}