package com.calculator.RPN;

import java.text.NumberFormat;
import java.util.Scanner;

public class Builder {
	
	static Scanner sc = new Scanner(System.in);
	static NumberFormat nf = NumberFormat.getInstance();
	public static void main(String[] args) {
		nf.setMaximumFractionDigits(5);
		System.out.print("Enter Expression: ");
		String expression = sc.nextLine();
		//false = radians true = degrees
		//qrtx(Y) - for roots to any degree
		try{
			System.out.println("Result: " + nf.format(ReversePolishNotation.run(expression, true)));
		}catch(IllegalArgumentException e){
			System.out.println("Compilation Error!");
		}
		
		//!!I may need to reset negativeHit after each ! is managed
	}
}

