package com.calculator.MatrixOperations;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;

import org.junit.Before;
import org.junit.Test;


public class MatrixOperationsTest {
	 MatrixOperations matrixOperations;
	
	@Before
	public void setUp()
	{
		matrixOperations = new MatrixOperations();
	}

	@Test
	public void testMatrixAddition(){
		int[][] m = new int[2][3], n = new int[2][3];
		m[0][0] = 0; m[0][1] = 1; m[0][2] = 2;
		m[1][0] = 9; m[1][1] = 8; m[1][2] = 7;
		
		n[0][0] = 6; n[0][1] = 5; n[0][2] = 4;
		n[1][0] = 3; n[1][1] = 4; n[1][2] = 5;
		
		int[][] result = new int[2][3];
		result[0][0] = 6; result[0][1] = 6; result[0][2] = 6;
		result[1][0] = 12; result[1][1] = 12; result[1][2] = 12;

		assertArrayEquals(result, matrixOperations.add(m, n));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testIllegalMatrixAddition(){
		int[][] m = new int[2][3], n = new int[3][2];
		m[0][0] = 0; m[0][1] = 1; m[0][2] = 2;
		m[1][0] = 9; m[1][1] = 8; m[1][2] = 7;
		
		n[0][0] = 6; n[0][1] = 5; 
		n[1][0] = 4; n[1][1] = 3;
		n[2][0] = 4; n[2][1] = 5;
		
		matrixOperations.add(m, n);
	}
	
	@Test
	public void testMatrixSubtraction(){
		int[][] m = new int[2][3], n = new int[2][3];
		m[0][0] = 0; m[0][1] = 1; m[0][2] = 2;
		m[1][0] = 9; m[1][1] = 8; m[1][2] = 7;
		
		n[0][0] = 6; n[0][1] = 5; n[0][2] = 4;
		n[1][0] = 3; n[1][1] = 4; n[1][2] = 5;
		
		int[][] result = new int[2][3];
		result[0][0] = -6; result[0][1] = -4; result[0][2] = -2;
		result[1][0] = 6; result[1][1] = 4; result[1][2] = 2;
		
		assertArrayEquals(result, matrixOperations.subtract(m, n));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testIllegalMatrixSubtraction(){
		int[][] m = new int[2][3], n = new int[3][2];
		m[0][0] = 0; m[0][1] = 1; m[0][2] = 2;
		m[1][0] = 9; m[1][1] = 8; m[1][2] = 7;
		
		n[0][0] = 6; n[0][1] = 5; 
		n[1][0] = 4; n[1][1] = 3;
		n[2][0] = 4; n[2][1] = 5;
		
		matrixOperations.subtract(m, n);
	}
	
	@Test
	public void testMatrixMultiplication(){
		int[][] m = new int[2][3], n = new int[3][2];
		m[0][0] = 0; m[0][1] = 1; m[0][2] = 2;
		m[1][0] = 9; m[1][1] = 8; m[1][2] = 7;
		
		n[0][0] = 6; n[0][1] = 5; 
		n[1][0] = 4; n[1][1] = 3;
		n[2][0] = 4; n[2][1] = 5;
		
		int[][] result = new int[2][2];
		result[0][0] = 12; result[0][1] = 13;
		result[1][0] = 114; result[1][1] = 104;
		
		assertArrayEquals(result, matrixOperations.multiply(m, n));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testIllegalMatrixMultiplication(){
		int[][] m = new int[2][3], n = new int[2][3];
		m[0][0] = 0; m[0][1] = 1; m[0][2] = 2;
		m[1][0] = 9; m[1][1] = 8; m[1][2] = 7;
		
		n[0][0] = 6; n[0][1] = 5; n[0][2] = 4;
		n[1][0] = 3; n[1][1] = 4; n[1][2] = 5;
		matrixOperations.multiply(m, n);
	}
	
	@Test
	public void testOutputMatrix(){
		int[][] m = new int[2][3];
		m[0][0] = 0; m[0][1] = 1; m[0][2] = 2;
		m[1][0] = 9; m[1][1] = 8; m[1][2] = 7;
		assertArrayEquals(m,matrixOperations.outputMatrix(m));
	}
	
	@Test
	public void testMainOutput(){
		
	}

}
