package com.calculator.MatrixOperations;

import java.util.Scanner;

public class MatrixOperations {
	private static Scanner sc = new Scanner(System.in);
	
	public static int[][] inputMatrix(int rows, int columns){
		int[][] inputMatrix = new int[rows][columns];
		for(int i = 0; i < rows; i++){
			for(int j = 0; j < columns; j++){
				inputMatrix[i][j] = sc.nextInt();
			}
		}
		return inputMatrix;
	}
	
	public static int[][] add(int[][] m, int[][] n){
		int rows = m.length;
		int columns = m[0].length;
		int[][] result = new int[rows][columns];
		if(m.length != n.length){
			System.out.println("Illegal matrix dimensions!");
		}
		else{
			for (int i = 0; i < rows; i++){
				for(int j = 0; j < columns; j++){
					result[i][j] = m[i][j] + n[i][j];
				}
			}
		}
		return result;
	}
	
	public static int[][] subtract(int[][] m, int[][] n){
		int rows = m.length;
		int columns = m[0].length;
		int[][] result = new int[rows][columns];
		if(m.length != n.length){
			System.out.println("Illegal matrix dimensions!");
		}
		else{
			for (int i = 0; i < rows; i++){
				for(int j = 0; j < columns; j++){
					result[i][j] = m[i][j] - n[i][j];
				}
			}
		}
		return result;
	}
	
	public static int[][] multiply(int[][] m, int[][] n){
		int[][] result = new int[m.length][n[0].length];
		if(m[0].length != n.length){
			System.out.println("Illegal matrix dimensions!");
		}
		else{
			
			for(int i = 0; i < result.length; i++){
				for(int j = 0; j < m.length; j++){
					for(int k = 0; k < m[0].length; k++){
						result[i][j] += m[i][k] * n[k][j];
					}
				}
			}
		}
		return result;
	}
	
	
	public static void outputMatrix(int[][] matrix){
		int rows = matrix.length;
		int columns = matrix[0].length;
		for(int i = 0; i < rows; i++){
			for(int j = 0; j < columns; j++){
				System.out.print(matrix[i][j] + " ");
			}
			System.out.println();
		}
	}

	public static void main(String[] args) {
		System.out.println("Enter the number of rows in first matrix(M): ");
		int rowsMatrixM = sc.nextInt();
		System.out.println("Enter the number of columns in first matrix(M): ");
		int columnsMatrixM = sc.nextInt();
		System.out.println("Enter the number of rows in second matrix(N): ");
		int rowsMatrixN = sc.nextInt();
		System.out.println("Enter the number of columns in second matrix(N): ");
		int columnsMatrixN = sc.nextInt();
		System.out.println("Enter first matrix(M) values:");
		int[][] m = inputMatrix(rowsMatrixM, columnsMatrixM);
		System.out.println("Enter second matrix(N) values:");
		int[][] n = inputMatrix(rowsMatrixN, columnsMatrixN);
		sc.nextLine();
		System.out.println("Please enter 'add', 'subtract', 'multiply' or 'all' for desired output.");
		switch(sc.next()){
		case "add": 
			int[][] sum = add(m,n);
			System.out.println("M + N = ");
			outputMatrix(sum);
			break;
			
		case "subtract":
			System.out.println("To subtract N from M, enter 'MN', to subtract M from N, enter 'NM'");
			if(sc.next().equalsIgnoreCase("MN")){
				int[][] differenceMN = subtract(m,n);
				System.out.println("M - N = ");
				outputMatrix(differenceMN);
			}
			else if(sc.next().equalsIgnoreCase("NM")){
				int[][] differenceNM = subtract(n,m);
				System.out.println("N - M = ");
				outputMatrix(differenceNM);
			}
			break;
			
		case "multiply":
			int[][] product = multiply(m,n);
			System.out.println("M * N = ");
			outputMatrix(product);
			break;
			
		case "all":
			int[][] sumAll = add(m,n);
			int[][] differenceMN = subtract(m,n);
			int[][] differenceNM = subtract(n,m);
			System.out.println("M + N = ");
			outputMatrix(sumAll);
			System.out.println();
			System.out.println("M - N = ");
			outputMatrix(differenceMN);
			System.out.println();
			System.out.println("N - M = ");
			outputMatrix(differenceNM);
			break;
		}
		
		/*int[][] sum = add(m,n);
		int[][] differenceMN = subtract(m,n);
		int[][] differenceNM = subtract(n,m);
		System.out.println("M + N = ");
		outputMatrix(sum);
		System.out.println();
		System.out.println("M - N = ");
		outputMatrix(differenceMN);
		System.out.println();
		System.out.println("N - M = ");
		outputMatrix(differenceNM);*/
	}
}
